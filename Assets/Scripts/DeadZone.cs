﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DeadZone : MonoBehaviour {
	public Text scorePlayerText;
	public Text scoreEnemyText;
	public SceneChanger sceneChanger;
	
	int scorePlayerQuantity;
	int scoreEnemyQuantity;

	public AudioSource ballAudio;

	private void OnTriggerEnter2D(Collider2D ball){
		ballAudio.Play();
		if(gameObject.tag == "Right"){
			scorePlayerQuantity++;
			scorePlayerText.text = scorePlayerQuantity.ToString();
		}else if(gameObject.CompareTag("Left")){
			scoreEnemyQuantity++;
			scoreEnemyText.text = scoreEnemyQuantity.ToString();
		}
		ball.GetComponent<BallBehaviour>().gameStarted = false;
		checkScore();

	}
	void checkScore(){
		if(scorePlayerQuantity >= 3){
			sceneChanger.ChangeSceneTo("WinScene");
		}else if(scoreEnemyQuantity >= 3){
			sceneChanger.ChangeSceneTo("LoseScene");
		}
	}
}
